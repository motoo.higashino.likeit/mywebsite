package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.Level;
import beans.User;

public class UserLevelDAO {
	//level_idでアカウント階級を取得 （ユーザー詳細の時に使う）
	public static Level getLevelByLevelId(User user) throws SQLException {
		Level level = new Level();
		Connection con = null;
		PreparedStatement st = null;
		try {

			con = DBManager.getConnection();
			st = con.prepareStatement("SLELCT level_name FROM t_user_level WHERE level_id = ?");
			st.setInt(1, user.getLevelId());
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				level.setLevel(rs.getString("level_name"));
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return level;

	}

}
