package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.User;

public class UserDAO {

	//暗号化処理
	public static String MD5(String password) {
		try {
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			String passwordMD5 = DatatypeConverter.printHexBinary(bytes);

			return passwordMD5;

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		return null;
	}

	//ログイン処理
	public static User login(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		User user = null;
		String passwordMD5 = MD5(password);
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT id,level_id,user_name,money FROM t_user_list WHERE login_id=? AND password=?");
			st.setString(1, loginId);
			st.setString(2, passwordMD5);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				user = new User(
						rs.getInt("id"),
						rs.getInt("level_id"),
						loginId,
						rs.getString("user_name"),
						rs.getInt("money"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return user;
	}

	//loginIdの有無を確認（登録時にloginIdが使えるか確認する用）
	public static boolean checkLoginId(String loginId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_user_list WHERE login_id = ?");
			st.setString(1, loginId);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//登録時にユーザー情報を書き込み
	public static void insertUser(User user) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		String passwordMD5 = MD5(user.getPassword());
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_user_list "
					+ "(level_id,login_id,user_name,password,mail_address,postal_code,address,tel,money,create_date,update_date)"
					+ " VALUES (1,?,?,?,?,?,?,?,0,now(),now())");
			st.setString(1, user.getLoginId());
			st.setString(2, user.getName());
			st.setString(3, passwordMD5);
			st.setString(4, user.getMailAddress());
			st.setString(5, user.getPostalCode());
			st.setString(6, user.getAddress());
			st.setString(7, user.getTel());
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	//ユーザー情報の更新 パスワードあり
	public static void updateUser7(User user) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		String passwordMD5 = MD5(user.getPassword());
		try {

			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user_list SET "
					+ "user_name=?,password=?,mail_address=?,postal_code=?,address=?,tel=?,update_date=now(),level_id=? "
					+ "WHERE id=?");
			st.setString(1, user.getName());
			st.setString(2, passwordMD5);
			st.setString(3, user.getMailAddress());
			st.setString(4, user.getPostalCode());
			st.setString(5, user.getAddress());
			st.setString(6, user.getTel());
			st.setInt(7, user.getLevelId());
			st.setInt(8, user.getId());
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ユーザー情報の更新 パスワードなし
	public static void updateUser6(User user)
			throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {

			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user_list "
					+ "SET user_name=?,mail_address=?,postal_code=?,address=?,tel=?,update_date=now(),level_id=? "
					+ "WHERE id=?");
			st.setString(1, user.getName());
			st.setString(2, user.getMailAddress());
			st.setString(3, user.getPostalCode());
			st.setString(4, user.getAddress());
			st.setString(5, user.getTel());
			st.setInt(6, user.getLevelId());
			st.setInt(7, user.getId());
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//全ユーザーリストを取得（管理者用）
	public static List<User> getUserList() throws SQLException {
		Connection con = null;
		List<User> userList = new ArrayList<User>();
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM t_user_list u INNER JOIN t_user_level l ON u.level_id = l.level_id WHERE id !=1");
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int levelId = rs.getInt("level_id");
				String levelName = rs.getString("level_name");
				String loginId = rs.getString("login_id");
				String name = rs.getString("user_name");
				String password = rs.getString("password");
				String mailAddress = rs.getString("mail_address");
				String postalCode = rs.getString("postal_code");
				String address = rs.getString("address");
				String tel = rs.getString("tel");
				int money = rs.getInt("money");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_Date");
				User user = new User(id, levelId, levelName, loginId, name, password, mailAddress, postalCode, address,
						tel,
						money,
						createDate, updateDate);
				userList.add(user);
			}
			return userList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ユーザーIDを使ってユーザー情報を取得
	public static User getUserByUserId(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		User user = null;

		try {

			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM t_user_list u INNER JOIN t_user_level l ON u.level_id = l.level_id WHERE id = ?");
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();

			while (rs.next()) {

				int levelId = rs.getInt("level_id");
				String levelName = rs.getString("level_name");
				String loginId = rs.getString("login_id");
				String name = rs.getString("user_name");
				String password = rs.getString("password");
				String mailAddress = rs.getString("mail_address");
				String postalCode = rs.getString("postal_code");
				String address = rs.getString("address");
				String tel = rs.getString("tel");
				int money = rs.getInt("money");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_Date");
				user = new User(id, levelId, levelName, loginId, name, password, mailAddress, postalCode, address, tel,
						money,
						createDate, updateDate);
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		return user;
	}

	//ユーザーの検索機能、検索項目「ログインID（部分検索）」「ユーザー名（部分検索）」「メールアドレス（部分検索）」
	public static List<User> searchUserList(String loginIdS, String userName, String mailAddressS) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		List<User> uList = new ArrayList<User>();

		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM t_user_list u INNER JOIN t_user_level l ON u.level_id = l.level_id WHERE id !=1";
			if (!loginIdS.equals("")) {
				sql += " AND login_id LIKE '%" + loginIdS + "%'";
			}
			if (!userName.equals("")) {
				sql += " AND user_name LIKE '%" + userName + "%'";
			}
			if (!mailAddressS.equals("")) {
				sql += " AND mail_address LIKE '%" + mailAddressS + "%'";
			}
			st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int levelId = rs.getInt("level_id");
				String levelName = rs.getString("level_name");
				String loginId = rs.getString("login_id");
				String name = rs.getString("user_name");
				String password = rs.getString("password");
				String mailAddress = rs.getString("mail_address");
				String postalCode = rs.getString("postal_code");
				String address = rs.getString("address");
				String tel = rs.getString("tel");
				int money = rs.getInt("money");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_Date");
				User user = new User(id, levelId, levelName, loginId, name, password, mailAddress, postalCode,
						address, tel, money,
						createDate, updateDate);
				uList.add(user);
			}
			return uList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//所持金更新（チャージ処理・支払い処理で使用）
	public static void updateMoney(int id, int money) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user_list SET money=? WHERE id = ?");
			st.setInt(1, money);
			st.setInt(2, id);
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	//ユーザー削除
	public static void deleteUser(User user) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM t_user_list WHERE id = ?");
			st.setInt(1, user.getId());
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
