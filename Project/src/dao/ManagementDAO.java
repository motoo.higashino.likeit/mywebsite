package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.Event;
import beans.User;

public class ManagementDAO {
	//ユーザーIDとイベントIDを紐づけして登録
	public static void insertUserIdAndEventId(int eventId, int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_management_user_event (event_id,user_id) VALUES (?,?)");
			st.setInt(1, eventId);
			st.setInt(2, userId);
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	//紐づけ情報を削除（イベントキャンセル処理で使用）
	public static void deleteUserIdAndEventId(int eventId,int userId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM t_management_user_event WHERE event_id = ? AND user_id = ?");
			st.setInt(1, eventId);
			st.setInt(2, userId);
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//参加ステータス確認（イベントIDとユーザーIDの組み合わせの有無を確認）
	public static boolean checkJoinStatus(int eventId,int userId)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_management_user_event WHERE event_id = ? AND user_id = ?");
			st.setInt(1, eventId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();
			if(rs.next()) {
				return true;
			}else {
				return false;
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//イベントIDでユーザーリストを取得(t_userとテーブル結合)　管理者・主催者用
	public static List<User> getUserByEventId(int eventId) throws SQLException {
		Connection con = null;
		List<User> userList = new ArrayList<User>();
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_management_user_event m "
					+ "INNER JOIN t_user_list t ON m.user_id=t.id "
					+ "INNER JOIN t_user_level l ON t.level_id = l.level_id "
					+ "WHERE event_id=?");
			st.setInt(1, eventId);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				int levelId = rs.getInt("level_id");
				String levelName = rs.getString("level_name");
				String loginId = rs.getString("login_id");
				String name = rs.getString("user_name");
				String password = rs.getString("password");
				String mailAddress = rs.getString("mail_address");
				String postalCode = rs.getString("postal_code");
				String address = rs.getString("address");
				String tel = rs.getString("tel");
				int money = rs.getInt("money");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_Date");
				User user = new User(id, levelId, levelName, loginId, name, password, mailAddress, postalCode, address,
						tel,
						money,
						createDate, updateDate);
				userList.add(user);

			}
			return userList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//ユーザーIDでイベントリストを取得(t_eventとテーブル結合)
	public static List<Event> getEventByUserId(int userId) throws SQLException, ParseException {
		Connection con = null;
		List<Event> eventList = new ArrayList<Event>();
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_management_user_event m "
					+ "INNER JOIN t_event_list t ON m.event_id=t.id "
					+ "WHERE user_id=?  ORDER BY t.start_date ASC");
			st.setInt(1, userId);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("event_id");
				String name = rs.getString("event_name");
				String tag = rs.getString("event_tag");
				int hostId = rs.getInt("host_id");
				String detail = rs.getString("detail");
				String image = rs.getString("image");
				int price = rs.getInt("price");
				String startDate = rs.getString("start_date");
				String endDate = rs.getString("end_date");

				//日程の表示形式を整える
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd  HH:mm");
				Date dSD = null;
				String sSD = null;
				Date dED = null;
				String sED = null;
				dSD = sdf.parse(startDate);
				sSD = f.format(dSD);
				dED = sdf.parse(endDate);
				sED = f.format(dED);

				Event event = new Event(id, name, tag, hostId, detail, image, price, startDate, endDate);

				//整えた日程を追加
				event.setsD(sSD);
				event.seteD(sED);

				eventList.add(event);
			}
			return eventList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
