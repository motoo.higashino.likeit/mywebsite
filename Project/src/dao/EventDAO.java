package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import base.DBManager;
import beans.Event;

public class EventDAO {

	//イベント情報の登録DAO
	public static void insertEvent(Event event) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("INSERT INTO t_event_list "
					+ "(event_name,event_tag,host_id,detail,image,price,start_date,end_date) "
					+ "VALUES(?,?,?,?,?,?,?,?)");
			st.setString(1, event.getName());
			st.setString(2, event.getTag());
			st.setInt(3, event.getHostId());
			st.setString(4, event.getDetail());
			st.setString(5, event.getImage());
			st.setInt(6, event.getPrice());
			st.setString(7, event.getStartDate());
			st.setString(8, event.getEndDate());
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

	}

	//イベントの検索機能・検索項目「イベント名（部分検索）」「イベントタグ（部分検索）」「開始日・終了日」
	public static List<Event> searchEvent(String eName, String eTag, String sDate, String eDate) throws SQLException, ParseException {
		Connection con = null;
		PreparedStatement st = null;
		List<Event> seList = new ArrayList<Event>();
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM t_event_list t WHERE id !=''";
			if (!eName.equals("")) {
				sql += " AND event_name LIKE '%" + eName + "%'";
			}
			if (!eTag.equals("")) {
				sql += " AND event_tag LIKE '%" + eTag + "%'";
			}
			if (!sDate.equals("")) {
				sql += " AND start_date >='" + sDate + "'";
			}
			if (!eDate.equals("")) {
				sql += " AND end_date <='" + eDate + "'";
			}
			sql += " ORDER BY t.start_date ASC";

			st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("event_name");
				String tag = rs.getString("event_tag");
				int hostId = rs.getInt("host_id");
				String detail = rs.getString("detail");
				String image = rs.getString("image");
				int price = rs.getInt("price");
				String startDate = rs.getString("start_date");
				String endDate = rs.getString("end_date");

				//日程の表示形式を整える
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd  HH:mm");
				Date dSD = null;
				String sSD = null;
				Date dED = null;
				String sED = null;
				dSD = sdf.parse(startDate);
				sSD = f.format(dSD);
				dED = sdf.parse(endDate);
				sED = f.format(dED);

				Event event = new Event(id, name, tag, hostId, detail, image, price, startDate, endDate);

				//整えた日程を追加
				event.setsD(sSD);
				event.seteD(sED);

				seList.add(event);
			}
			return seList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//イベント情報の取得
	public static Event getEventByEventId(int eventId) throws SQLException, ParseException {
		Connection con = null;
		Event event = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_event_list WHERE id=?");
			st.setInt(1, eventId);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("event_name");
				String tag = rs.getString("event_tag");
				int hostId = rs.getInt("host_id");
				String detail = rs.getString("detail");
				String image = rs.getString("image");
				int price = rs.getInt("price");
				String startDate = rs.getString("start_date");
				String endDate = rs.getString("end_date");

				//日程の表示形式を整える
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd  HH:mm");
				Date dSD = null;
				String sSD = null;
				Date dED = null;
				String sED = null;
				dSD = sdf.parse(startDate);
				sSD = f.format(dSD);
				dED = sdf.parse(endDate);
				sED = f.format(dED);

				event = new Event(id, name, tag, hostId, detail, image, price, startDate, endDate);

				//整えた日程を追加
				event.setsD(sSD);
				event.seteD(sED);

			}
			return event;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//主催者IDでイベントリストを取得
	public static List<Event> getEventByHostId(int hostId) throws SQLException, ParseException {
		List<Event> heList = new ArrayList<Event>();
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_event_list t WHERE host_id=? ORDER BY t.start_date ASC");
			st.setInt(1, hostId);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("event_name");
				String tag = rs.getString("event_tag");
				String detail = rs.getString("detail");
				String image = rs.getString("image");
				int price = rs.getInt("price");
				String startDate = rs.getString("start_date");
				String endDate = rs.getString("end_date");

				//日程の表示形式を整える
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd  HH:mm");
				Date dSD = null;
				String sSD = null;
				Date dED = null;
				String sED = null;
				dSD = sdf.parse(startDate);
				sSD = f.format(dSD);
				dED = sdf.parse(endDate);
				sED = f.format(dED);

				Event event = new Event(id, name, tag, hostId, detail, image, price, startDate, endDate);

				//整えた日程を追加
				event.setsD(sSD);
				event.seteD(sED);

				heList.add(event);
			}
			return heList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//これから開催予定のイベントリストを取得
	public static List<Event> getEventWillHost() throws SQLException, ParseException {
		List<Event> wheList = new ArrayList<Event>();
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_event_list t WHERE start_date > now() ORDER BY t.start_date ASC");
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("event_name");
				String tag = rs.getString("event_tag");
				int hostId = rs.getInt("host_id");
				String detail = rs.getString("detail");
				String image = rs.getString("image");
				int price = rs.getInt("price");
				String startDate = rs.getString("start_date");
				String endDate = rs.getString("end_date");

				//日程の表示形式を整える
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
				SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd  HH:mm");
				Date dSD = null;
				String sSD = null;
				Date dED = null;
				String sED = null;
				dSD = sdf.parse(startDate);
				sSD = f.format(dSD);
				dED = sdf.parse(endDate);
				sED = f.format(dED);

				Event event = new Event(id, name, tag, hostId, detail, image, price, startDate, endDate);

				//整えた日程を追加
				event.setsD(sSD);
				event.seteD(sED);

				wheList.add(event);
			}
			return wheList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//イベントの更新機能
	public static void updateEvent(Event event)throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_event_list SET "
					+ "event_name = ?, event_tag = ?, image = ?, start_date = ?, end_date = ?,"
					+ "price = ? ,detail = ? WHERE id = ?");
			st.setString(1, event.getName());
			st.setString(2, event.getTag());
			st.setString(3, event.getImage());
			st.setString(4, event.getStartDate());
			st.setString(5, event.getEndDate());
			st.setInt(6, event.getPrice());
			st.setString(7, event.getDetail());
			st.setInt(8, event.getId());
			st.executeUpdate();

		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//イベント添付画像の有無確認
	public static boolean checkImage(String image) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT image FROM t_event_list WHERE image = ?");
			st.setString(1, image);
			ResultSet rs = st.executeQuery();
			if(rs.next()) {
				return true;
			}else {
				return false;
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//イベント削除
	public static void deleteEvent(int id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM t_event_list WHERE id=?");
			st.setInt(1, id);
			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
}
