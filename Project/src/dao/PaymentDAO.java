package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBManager;

public class PaymentDAO {
	//支払い時に、情報を書き込み（記録用）
	public static void insertPayment(int eventId, int userId, int payment) throws SQLException {
		Connection conn = null;
		PreparedStatement st = null;
		try {
			conn = DBManager.getConnection();

			String sql = "INSERT INTO t_payment (event_id,user_id,payment,pay_date) "
					+ "VALUES (?,?,?,now())";

			st = conn.prepareStatement(sql);
			st.setInt(1, eventId);
			st.setInt(2, userId);
			st.setInt(3, payment);

			st.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}

	}

}
