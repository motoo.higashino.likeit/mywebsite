package me;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class IndexUser
 */
@WebServlet("/IndexUser")
public class IndexUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IndexUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		//登録・編集から戻った際にセッションを削除
		session.removeAttribute("dUser");
		session.removeAttribute("eUser");
		session.removeAttribute("event");
		session.removeAttribute("rEvent");
		session.removeAttribute("eEvent");
		session.removeAttribute("hostName");

		try {

			//ログインセッションが無い場合ログイン画面へ遷移
			User user = (User) session.getAttribute("user");
			if (user == null) {
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}

			//登録されている全ユーザーリストを取得
			List<User> uList = UserDAO.getUserList();

			//リストの有無を確認
			if (uList.size() == 0) {
				uList = null;
			}

			//リストをセット
			request.setAttribute("uList", uList);

			//ユーザー一覧へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/indexuser.jsp").forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//パラメーター取得
			String loginId = request.getParameter("loginId");
			String userName = request.getParameter("userName");
			String mailAddress = request.getParameter("mailAddress");

			//ユーザー検索機能
			List<User> uList = UserDAO.searchUserList(loginId, userName, mailAddress);

			//リストの有無を確認
			if ( uList.size() == 0) {
				uList = null;
			}

			//検索結果とセット
			request.setAttribute("uList", uList);

			//ユーザー一覧へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/indexuser.jsp").forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
