package me;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Event;
import beans.User;
import dao.EventDAO;
import dao.ManagementDAO;
import dao.UserDAO;

/**
 * Servlet implementation class JoinEvent
 */
@WebServlet("/JoinEvent")
public class JoinEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public JoinEvent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			
			//ログインセッションが無い場合ログイン画面へ遷移
			User user = (User) session.getAttribute("user");
			if (user == null) {
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}
			
			//イベント情報取得
			int id = Integer.parseInt(request.getParameter("id"));
			Event event = EventDAO.getEventByEventId(id);
			session.setAttribute("jEvent", event);

			//参加確認画面へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/joinevent.jsp").forward(request, response);

		}catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//セッション呼び出し
			Event e = (Event)session.getAttribute("jEvent");
			User u = (User)session.getAttribute("user");

			//変数設定
			int eId = e.getId();
			int uId = u.getId();
			int ePrice = e.getPrice();
			int uMoney = u.getMoney();

			//参加状況確認
			if(ManagementDAO.checkJoinStatus(eId, uId)) {
				request.setAttribute("errMsg", "既に参加予約しています");
				request.getRequestDispatcher("WEB-INF/jsp/joinevent.jsp").forward(request, response);
			}else if(ePrice>uMoney) {
				//所持金不足処理
				int needMoney = ePrice - uMoney;
				request.setAttribute("errMsg", "所持金が足りません");
				request.setAttribute("needMoney", "あと"+needMoney+"円足りません　チャージして下さい");
				request.getRequestDispatcher("WEB-INF/jsp/joinevent.jsp").forward(request, response);
			}else {
				//イベント参加登録処理
				ManagementDAO.insertUserIdAndEventId(eId, uId);

				//料金支払い処理
				int newUMoney = uMoney - ePrice;

				//データベースに更新
				UserDAO.updateMoney(uId, newUMoney);

				//セッションを更新
				u.setMoney(newUMoney);
				session.setAttribute("user", u);

				//参加イベントセッションの削除
				request.setAttribute("jEvent", e);
				session.removeAttribute("jEvent");

				//確認画面へ遷移
				request.getRequestDispatcher("WEB-INF/jsp/joineventresult.jsp").forward(request, response);

			}

		}catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
