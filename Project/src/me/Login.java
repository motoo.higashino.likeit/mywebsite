package me;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();

		//ログアウト時のメッセージをセット
		request.setAttribute("logoutMsg", session.getAttribute("logoutMsg"));
		session.removeAttribute("logoutMsg");

		//ログインセッションがある場合イベント一覧画面へ遷移
		User user = (User) session.getAttribute("user");
		if (user != null) {
			response.sendRedirect("IndexEvent");

			//ログインセッションが無い場合ログイン画面へ遷移
		} else {
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

			//ログインIDとパスワードを引数にログインを確認
			String loginId = request.getParameter("loginId");
			String password = request.getParameter("password");

			//未入力の場合のエラー処理
			if (loginId.equals("")) {
				request.setAttribute("errMsg", "ログインIDが入力されていません");
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			} else if (!loginId.equals("") && password.equals("")) {
				request.setAttribute("loginId", loginId);
				request.setAttribute("errMsg", "パスワードが入力されていません");
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}

			//ログイン処理
			User user = UserDAO.login(loginId, password);

			//ログイン成功時　イベント一覧画面へ
			if (user != null) {
				session.setAttribute("user", user);
				response.sendRedirect("IndexEvent");

				//ログイン失敗時にエラーメッセージを返す
			} else {
				if (UserDAO.checkLoginId(loginId)) {
					//ログインIDが正しく、パスワードが違う場合
					request.setAttribute("loginId", loginId);
					request.setAttribute("errMsg", "パスワードが正しくありません");
				} else {
					//ログインIDが存在しない場合
					request.setAttribute("errMsg", "ログインIDが正しくありません");
				}
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
