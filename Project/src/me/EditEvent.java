package me;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.Event;
import beans.User;
import dao.EventDAO;
import dao.UserDAO;

/**
 * Servlet implementation class EditEvent
 */
@WebServlet("/EditEvent")
@MultipartConfig(location = "C:\\Users\\MayumiのPC\\Documents\\mywebsite\\Project\\WebContent\\img", maxFileSize = 1048576)
public class EditEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditEvent() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//ログインセッションが無い場合ログイン画面へ遷移
			User user = (User) session.getAttribute("user");
			if (user == null) {
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}

			//変数初期化
			Event eEvent = null;

			//編集内容のセッションが無い場合（イベント一覧から遷移してきた場合）イベント情報を取得
			if (session.getAttribute("eEvent") == null && session.getAttribute("hostName") == null) {

				//イベントID取得
				int eId = Integer.parseInt(request.getParameter("id"));

				//イベント情報を取得
				eEvent = EventDAO.getEventByEventId(eId);

				//主催者名を取得
				User u = UserDAO.getUserByUserId(eEvent.getHostId());

				//日時表示を変換ーーー開始

				//変数初期化
				String startDate = eEvent.getStartDate();
				String endDate = eEvent.getEndDate();
				Date sESD = null;
				String sD = null;
				Date sEED = null;
				String eD = null;

				//インスタンス生成
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS.S");
				SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

				//日時の型をString型（画面表示用）に変換
				sESD = sdf.parse(startDate);
				sD = f.format(sESD);
				sEED = sdf.parse(endDate);
				eD = f.format(sEED);

				//イベントインスタンスにセット
				eEvent.setsD(sD);
				eEvent.seteD(eD);

				//日時表示を変換ーーー終了

				//開催者名をセッションにセット
				session.setAttribute("hostName", u.getName());

				//編集内容のセッションがある場合
			} else {

				//イベントセッションを取得
				eEvent = (Event) session.getAttribute("eEvent");

				//編集内容を上書き
				eEvent.setsD(eEvent.getStartDate());
				eEvent.seteD(eEvent.getEndDate());

			}

			//イベントをセッションにセット
			session.setAttribute("eEvent", eEvent);

			//イベント編集画面に遷移
			request.getRequestDispatcher("WEB-INF/jsp/editevent.jsp").forward(request, response);

		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//パラメーターを取得
			String name = request.getParameter("name");
			String tag = request.getParameter("tag");
			String startDate = request.getParameter("startDate");
			String endDate = request.getParameter("endDate");
			String price = request.getParameter("price");
			String detail = request.getParameter("detail");

			//画像ファイル保存処理
			Part part = request.getPart("image");
			String eImage = this.getFileName(part);
			boolean imgStatus = false;
			if (eImage.equals("")) {
				//画像変更されなかった場合
			} else if (EventDAO.checkImage(eImage)) {
				//既に画像が保存されていた場合は保存しない
				imgStatus = true;
			} else {
				//新しい画像が保存された場合は保存を実行する
				part.write(eImage);
				imgStatus = true;
			}

			//エラー処理
			//値段に文字が入った場合の処理
			int iEPrice = 0;
			try {

				if (!price.equals("")) {
					iEPrice = Integer.parseInt(price);
				}
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("errMsg", "参加費を確認してください");
				request.getRequestDispatcher("WEB-INF/jsp/editevent.jsp").forward(request, response);
			}

			//日程の変換ーーー開始

			//インスタンス生成
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
			SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd  HH:mm");

			//変数初期化
			Date sESD = null;
			String sD = null;
			Date sEED = null;
			String eD = null;

			//日時の入力状況別の処理

			//日時が入力された場合　日時の方を変換
			if (!startDate.equals("")) {
				sESD = sdf.parse(startDate);
				sD = f.format(sESD);

				//日時が入力されなかった場合
			} else {
				sESD = null;
			}

			//日時が入力された場合　日時の方を変換
			if (!endDate.equals("")) {
				sEED = sdf.parse(endDate);
				eD = f.format(sEED);

				//日時が入力されなかった場合
			} else {
				sEED = null;
			}

			//日程の変換ーーー終了

			//イベント型にセット
			Event e = (Event) session.getAttribute("eEvent");
			e.setName(name);
			e.setTag(tag);
			//画像は更新された場合のみ
			if (!eImage.equals("")) {
				e.setImage(eImage);
			}
			e.setStartDate(startDate);
			e.setsD(sD);
			e.setEndDate(endDate);
			e.seteD(eD);
			e.setPrice(iEPrice);
			e.setDetail(detail);

			//未入力項目の確認・エラー処理
			if (name.equals("") || tag.equals("") ||
					startDate.equals("") || endDate.equals("") || price.equals("") || detail.equals("")) {
				request.setAttribute("errMsg", "未入力の項目があります");
				request.setAttribute("rEvent", e);
				request.getRequestDispatcher("WEB-INF/jsp/editevent.jsp").forward(request, response);
			}

			//日程矛盾の確認・エラー処理
			if (sESD.after(sEED)) {
				request.setAttribute("errMsg", "日程をご確認下さい");
				request.setAttribute("rEvent", e);
				request.getRequestDispatcher("WEB-INF/jsp/editevent.jsp").forward(request, response);
			}

			//確認画面へ遷移
			session.setAttribute("eEvent", e);
			request.setAttribute("imgStatus", imgStatus);
			request.getRequestDispatcher("WEB-INF/jsp/editeventconfirm.jsp").forward(request, response);

		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	//画像ファイルの保存処理メソッド
	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
