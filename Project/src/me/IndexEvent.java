package me;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Event;
import beans.User;
import dao.EventDAO;
import dao.ManagementDAO;

/**
 * Servlet implementation class IndexEvent
 */
@WebServlet("/IndexEvent")
public class IndexEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IndexEvent() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		//登録・編集から戻った際にセッションを削除
		session.removeAttribute("dUser");
		session.removeAttribute("eUser");
		session.removeAttribute("event");
		session.removeAttribute("rEvent");
		session.removeAttribute("eEvent");
		session.removeAttribute("hostName");

		//ログインセッションが無い場合ログイン画面へ遷移
		User user = (User) session.getAttribute("user");
		if (user == null) {
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		}

		try {
			//ログインユーザーの主催するイベントリストを取得
			List<Event> heList = EventDAO.getEventByHostId(user.getId());
			//リストのnullを確認
			if (heList == null || heList.size() == 0) {
				heList = null;
			}
			request.setAttribute("heList", heList);

			//ログインユーザーが参加予定のイベントリストを取得
			List<Event> jeList = ManagementDAO.getEventByUserId(user.getId());
			//リストのnullを確認
			if (jeList == null || jeList.size() == 0) {
				jeList = null;
			}
			request.setAttribute("jeList", jeList);

			//これから開催予定（startDate>now()）のイベントリストを取得
			List<Event> wheList = EventDAO.getEventWillHost();
			//リストのnullを確認
			if (wheList == null || wheList.size() == 0) {
				wheList = null;
			}
			request.setAttribute("wheList", wheList);

			//金額表示を変換
			NumberFormat nfNum = NumberFormat.getNumberInstance();
			String uMoney = nfNum.format(user.getMoney());
			request.setAttribute("uMoney", uMoney);

			//イベント一覧に遷移
			request.getRequestDispatcher("WEB-INF/jsp/indexevent.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		//検索機能
		try {
			//ユーザー情報を取得
			User user = (User) session.getAttribute("user");

			//ログインユーザーの主催するイベントリストを取得
			List<Event> heList = EventDAO.getEventByHostId(user.getId());
			//リストのnullを確認
			if (heList == null || heList.size() == 0) {
				heList = null;
			}
			request.setAttribute("heList", heList);

			//ログインユーザーが参加予定のイベントリストを取得
			List<Event> jeList = ManagementDAO.getEventByUserId(user.getId());
			//リストのnullを確認
			if (jeList == null || jeList.size() == 0) {
				jeList = null;
			}
			request.setAttribute("jeList", jeList);

			//検索パラメーター取得
			String eName = request.getParameter("eventName");
			String eTag = request.getParameter("eventTag");
			String sDate = request.getParameter("startDate");
			String eDate = request.getParameter("endDate");

			//検索リスト取得・セット
			List<Event> wheList = EventDAO.searchEvent(eName, eTag, sDate, eDate);
			request.setAttribute("wheList", wheList);

			//金額表示を変換
			NumberFormat nfNum = NumberFormat.getNumberInstance();
			String uMoney = nfNum.format(user.getMoney());
			request.setAttribute("uMoney", uMoney);

			//イベント一覧へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/indexevent.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

}
