package me;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.Event;
import beans.User;

/**
 * Servlet implementation class RegistEvent
 */
@WebServlet("/RegistEvent")
@MultipartConfig(location = "C:\\Users\\MayumiのPC\\Documents\\mywebsite\\Project\\WebContent\\img", maxFileSize = 1048576)
public class RegistEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistEvent() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		//ログインセッションが無い場合ログイン画面へ遷移
		User user = (User) session.getAttribute("user");
		if (user == null) {
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		}

		//イベント登録画面へ遷移
		request.getRequestDispatcher("WEB-INF/jsp/registevent.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			String eName = request.getParameter("eName");
			String eTag = request.getParameter("eTag");
			int eHostId = Integer.parseInt(request.getParameter("eHostId"));
			String eSD = request.getParameter("eSD");
			String eED = request.getParameter("eED");
			String sEPrice = request.getParameter("ePrice");
			String eDetail = request.getParameter("eDetail");

			//画像保存
			Part part = request.getPart("eImage");
			String eImage = this.getFileName(part);
			part.write(eImage);

			//値段の変換・値段に文字が入った場合のエラー処理
			int iEPrice = 0;
			try {

				if (!sEPrice.equals("")) {
					iEPrice = Integer.parseInt(sEPrice);
				}
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("errMsg", "参加費を確認してください");
				request.getRequestDispatcher("WEB-INF/jsp/registevent.jsp").forward(request, response);
			}

			//日程の変換ーーー開始

			//インスタンス生成
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
			SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd  HH:mm");

			//変数初期化
			Date sESD = null;
			String sD = null;
			Date sEED = null;
			String eD = null;

			//日時の入力状況別に処理

			//日時が入力された場合
			if (!eSD.equals("")) {
				sESD = sdf.parse(eSD);
				sD = f.format(sESD);

				//日時が入力されなかった場合
			} else {
				sESD = null;
			}

			//日時が入力された場合
			if (!eED.equals("")) {
				sEED = sdf.parse(eED);
				eD = f.format(sEED);

				//日時が入力されなかった場合
			} else {
				sEED = null;
			}

			//日程の変換ーーー終了

			//イベント型にセット
			Event e = new Event();
			e.setName(eName);
			e.setTag(eTag);
			e.setImage(eImage);
			e.setHostId(eHostId);
			e.setStartDate(eSD);
			e.setsD(sD);
			e.setEndDate(eED);
			e.seteD(eD);
			e.setPrice(iEPrice);
			e.setDetail(eDetail);

			//未入力項目の確認・エラー処理
			if (eName.equals("") || eTag.equals("") || eImage.equals("") || eHostId == 0 ||
					eSD.equals("") || eED.equals("") || sEPrice.equals("") || eDetail.equals("")) {
				request.setAttribute("errMsg", "未入力の項目があります");
				request.setAttribute("rEvent", e);
				request.getRequestDispatcher("WEB-INF/jsp/registevent.jsp").forward(request, response);
			}

			//日程矛盾の確認・エラー処理
			if (sESD.after(sEED)) {
				request.setAttribute("errMsg", "日程をご確認下さい");
				request.setAttribute("rEvent", e);
				request.getRequestDispatcher("WEB-INF/jsp/registevent.jsp").forward(request, response);
			}

			//確認画面へ遷移
			session.setAttribute("rEvent", e);
			request.getRequestDispatcher("WEB-INF/jsp/registeventconfirm.jsp").forward(request, response);

		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
