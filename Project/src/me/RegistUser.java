package me;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class RegistUser
 */
@WebServlet("/RegistUser")
public class RegistUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/registuser.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//パラメーター取得
			String loginId = request.getParameter("loginId");
			String UserName = request.getParameter("UserName");
			String password1 = request.getParameter("password1");
			String password2 = request.getParameter("password2");
			String mailAddress1 = request.getParameter("mailAddress1");
			String mailAddress2 = request.getParameter("mailAddress2");
			String postalCode = request.getParameter("postalCode");
			String address = request.getParameter("address");
			String tel = request.getParameter("tel");

			//パラメーターのセット
			User rUser = new User();
			rUser.setLoginId(loginId);
			rUser.setName(UserName);
			rUser.setPassword(password1);
			rUser.setMailAddress(mailAddress1);
			rUser.setPostalCode(postalCode);
			rUser.setAddress(address);
			rUser.setTel(tel);
			session.setAttribute("rUser", rUser);

			//入力パラメーターの確認

			//未入力の確認
			if (loginId.equals("") || UserName.equals("") || password1.equals("") || password2.equals("") ||
					mailAddress1.equals("") || mailAddress2.equals("") || postalCode.equals("") ||
					address.equals("") || tel.equals("")) {
				request.setAttribute("errMsg", "未入力の項目があります");
				request.getRequestDispatcher("WEB-INF/jsp/registuser.jsp").forward(request, response);
			}

			//loginIdの確認
			if (UserDAO.checkLoginId(loginId)) {
				request.setAttribute("errMsg", "そのログインIDはすでに使われています");
				request.getRequestDispatcher("WEB-INF/jsp/registuser.jsp").forward(request, response);
			}

			//パスワード一致の確認
			if (!password1.equals(password2)) {
				request.setAttribute("errMsg", "パスワードが一致しません");
				request.getRequestDispatcher("WEB-INF/jsp/registuser.jsp").forward(request, response);
			}

			request.getRequestDispatcher("WEB-INF/jsp/registuserconfirm.jsp").forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
