package me;

import java.io.IOException;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class ChargrMoneyConfirm
 */
@WebServlet("/ChargeMoneyConfirm")
public class ChargeMoneyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChargeMoneyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//パラメーター取得
			Number number = NumberFormat.getInstance().parse(request.getParameter("cMoney"));
			int cMoney = number.intValue();

			//セッションから所持金を取得
			User user = (User) session.getAttribute("user");
			int wMoney = user.getMoney();

			//チャージ計算後の金額をセッション・DBに保存
			int upMoney = wMoney + cMoney;
			user.setMoney(upMoney);
			UserDAO.updateMoney(user.getId(), upMoney);


			//金額表示を変換
			NumberFormat nfNum = NumberFormat.getNumberInstance();
			String uMoney = nfNum.format(user.getMoney());
			String scMoney = nfNum.format(cMoney);

			//金額をセット
			request.setAttribute("uMoney", uMoney);
			request.setAttribute("scMoney", scMoney);

			//セッションを更新
			session.setAttribute("user", user);

			//チャージ結果画面に遷移
			request.getRequestDispatcher("WEB-INF/jsp/chargemoneyresult.jsp").forward(request, response);

		}catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
