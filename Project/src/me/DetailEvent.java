package me;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Event;
import beans.User;
import dao.EventDAO;
import dao.ManagementDAO;
import dao.UserDAO;

/**
 * Servlet implementation class DetailEvent
 */
@WebServlet("/DetailEvent")
public class DetailEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailEvent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

			//ログインセッションが無い場合ログイン画面へ遷移
			User user = (User) session.getAttribute("user");
			if (user == null) {
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}

			//イベント編集から戻った際のセッション削除
			if(session.getAttribute("eEvent")!=null) {
				session.removeAttribute("eEvent");
			}
			if(session.getAttribute("hostName")!=null) {
				session.removeAttribute("hostName");
			}

			//イベント情報取得
			int id = Integer.parseInt(request.getParameter("id"));
			Event event = EventDAO.getEventByEventId(id);
			request.setAttribute("event", event);

			//主催者名　取得
			User hUser = UserDAO.getUserByUserId(event.getHostId());
			String uName = hUser.getName();
			request.setAttribute("uName", uName);

			//参加者リストを取得
			List<User> uList = ManagementDAO.getUserByEventId(id);
			if(uList.size()==0) {
				uList=null;
			}
			request.setAttribute("uList",uList);


			//予約ステータスを確認（jsp表示制御用の変数）
			boolean joinStatus = false;
			if(ManagementDAO.checkJoinStatus(id, user.getId())) {
				joinStatus = true;
			}
			request.setAttribute("joinStatus", joinStatus);

			//イベント詳細画面へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/detailevent.jsp").forward(request, response);

		}catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
