package me;

import java.io.IOException;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

/**
 * Servlet implementation class ChargeMoney
 */
@WebServlet("/ChargeMoney")
public class ChargeMoney extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChargeMoney() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//金額表示を変換
			User user = (User)session.getAttribute("user");
			NumberFormat nfNum = NumberFormat.getNumberInstance();
			String uMoney = nfNum.format(user.getMoney());

			//金額をセットし、チャージ画面へ遷移
			request.setAttribute("uMoney", uMoney);
			request.getRequestDispatcher("WEB-INF/jsp/chargemoney.jsp").forward(request, response);

		}catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			//パラメーターを取得
			String cMoney = request.getParameter("cMoney");

			//金額表示を変換
			User user = (User)session.getAttribute("user");
			NumberFormat nfNum = NumberFormat.getNumberInstance();
			String uMoney = nfNum.format(user.getMoney());
			String scMoney = nfNum.format(Integer.parseInt(cMoney));

			//金額をセットし、確認画面へ遷移
			request.setAttribute("uMoney", uMoney);
			request.setAttribute("scMoney", scMoney);

			//金額が選択されなかった場合のエラー処理
			if(cMoney==null) {
				request.setAttribute("errMsg", "金額を選択してください");
				request.getRequestDispatcher("WEB-INF/jsp/chargemoney.jsp").forward(request, response);
			}

			request.getRequestDispatcher("WEB-INF/jsp/chargemoneyconfirm.jsp").forward(request, response);

		}catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
