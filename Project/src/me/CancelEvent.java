package me;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Event;
import beans.User;
import dao.EventDAO;
import dao.ManagementDAO;

/**
 * Servlet implementation class cancelevent
 */
@WebServlet("/CancelEvent")
public class CancelEvent extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CancelEvent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			
			//ログインセッションが無い場合ログイン画面へ遷移
			User user = (User) session.getAttribute("user");
			if (user == null) {
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}
			
			//イベント情報取得
			int id = Integer.parseInt(request.getParameter("id"));
			Event event = EventDAO.getEventByEventId(id);
			session.setAttribute("cEvent", event);

			//キャンセル確認画面へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/cancelevent.jsp").forward(request, response);

		}catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//イベント情報取得
			//ユーザー情報取得
			User u = (User) session.getAttribute("user");
			Event e = (Event) session.getAttribute("cEvent");

			//キャンセル処理
			ManagementDAO.deleteUserIdAndEventId(e.getId(), u.getId());

			//リクエストセット・セッション削除
			request.setAttribute("cEvent", e);
			session.removeAttribute("cEvent");

			//確認画面へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/canceleventresult.jsp").forward(request, response);

		}catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}


	}

}
