package me;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Event;
import beans.User;
import dao.EventDAO;

/**
 * Servlet implementation class EditEventResult
 */
@WebServlet("/EditEventResult")
public class EditEventResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditEventResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//ログインセッションが無い場合ログイン画面へ遷移
			User user = (User) session.getAttribute("user");
			if (user == null) {
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}


		}catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//セッションの取得
			Event eEvent = (Event)session.getAttribute("eEvent");

			//イベント更新処理
			EventDAO.updateEvent(eEvent);

			//更新情報のセット
			request.setAttribute("eEventName", eEvent.getName());

			//セッションの削除
			session.removeAttribute("eEvent");
			session.removeAttribute("hostName");

			//更新結果画面へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/editeventresult.jsp").forward(request, response);

		}catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
