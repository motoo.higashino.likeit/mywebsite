package me;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class EditUser
 */
@WebServlet("/EditUser")
public class EditUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//ログインセッションが無い場合ログイン画面へ遷移
			User user = (User) session.getAttribute("user");
			if (user == null) {
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}

			//修正から流れてきた場合の処理（修正中の情報を取得）
			User eUser = (User)session.getAttribute("eUser");

			//セッションが無い場合
			if(eUser==null) {
				//ユーザー情報取得・セッションにセット
				User u = (User) session.getAttribute("user");
				User nEUser = UserDAO.getUserByUserId(u.getId());
				session.setAttribute("eUser", nEUser);
			}

			//ユーザー登録内容変更画面に遷移
			request.getRequestDispatcher("WEB-INF/jsp/edituser.jsp").forward(request, response);

		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {
			//ユーザーセッション取得
			User eUser = (User) session.getAttribute("eUser");

			//パラメーター取得
			String name = request.getParameter("name");
			String password1 = request.getParameter("password1");
			String password2 = request.getParameter("password2");
			String mailAddress1 = request.getParameter("mailAddress1");
			String mailAddress2 = request.getParameter("mailAddress2");
			String postalCode = request.getParameter("postalCode");
			String address = request.getParameter("address");
			String tel = request.getParameter("tel");

			//ユーザーインスタンスにパラメーターをセット
			eUser.setName(name);
			eUser.setPassword(password1);
			eUser.setMailAddress(mailAddress1);
			eUser.setPostalCode(postalCode);
			eUser.setAddress(address);
			eUser.setTel(tel);

			//エラー処理

			//パスワードが不一致の場合
			if (!password1.equals(password2)) {
				request.setAttribute("errMsg", "パスワードが一致していません");
				request.getRequestDispatcher("WEB-INF/jsp/edituser.jsp").forward(request, response);

				//メールアドレスが不一致の場合
			} else if (!mailAddress1.equals(mailAddress2)) {
				request.setAttribute("errMsg", "メールアドレスが一致していません");
				request.getRequestDispatcher("WEB-INF/jsp/edituser.jsp").forward(request, response);

				//未入力がある場合
			} else if (name.equals("") || mailAddress1.equals("") || mailAddress2.equals("") || postalCode.equals("")
					|| address.equals("") || tel.equals("")) {
				request.setAttribute("errMsg", "未入力の項目があります");
				request.getRequestDispatcher("WEB-INF/jsp/edituser.jsp").forward(request, response);

				//エラーがない場合
			} else {

				//パスワード更新状態メッセージをセット（jsp表示制御用）

				//パスワードあり
				if (!password1.equals("")) {
					request.setAttribute("passwordStatus", "新しいパスワード");

					//パスワードなし
				} else {
					request.setAttribute("passwordStatus", "変更なし");
				}

				//確認画面へ遷移
				session.setAttribute("eUser", eUser);
				request.getRequestDispatcher("WEB-INF/jsp/edituserconfirm.jsp").forward(request, response);
			}

		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
