package me;

import java.io.IOException;
import java.sql.SQLException;
import java.text.NumberFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class DetailUser
 */
@WebServlet("/DetailUser")
public class DetailUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

			//ログインセッションが無い場合ログイン画面へ遷移
			User user = (User) session.getAttribute("user");
			if (user == null) {
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}

			//ユーザーの情報を取得・セット
			User dUser = UserDAO.getUserByUserId(user.getId());

			//所持金の表示を変換
			NumberFormat nfNum = NumberFormat.getNumberInstance();
			String uMoney = nfNum.format(dUser.getMoney());

			//データをセット
			request.setAttribute("uMoney", uMoney);
			request.setAttribute("dUser", dUser);

			//ユーザー詳細画面へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/detailuser.jsp").forward(request, response);

		}catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
