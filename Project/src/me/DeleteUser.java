package me;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDAO;

/**
 * Servlet implementation class DeleteUser
 */
@WebServlet("/DeleteUser")
public class DeleteUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//ログインセッションが無い場合ログイン画面へ遷移
			User user = (User) session.getAttribute("user");
			if (user == null) {
				request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
			}

			//ユーザー情報の取得・セット
			User dUser = UserDAO.getUserByUserId(Integer.parseInt(request.getParameter("id")));
			session.setAttribute("dUser", dUser);

			//ユーザー削除確認画面へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/deleteuser.jsp").forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			//ユーザー情報を取得・結果表示用にセット
			User dUser = (User)session.getAttribute("dUser");
			request.setAttribute("uName", dUser.getName());

			//ユーザー削除処理
			UserDAO.deleteUser(dUser);

			//ユーザー削除結果画面へ遷移
			request.getRequestDispatcher("WEB-INF/jsp/deleteuserresult.jsp").forward(request, response);

		}catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
