package beans;

import java.io.Serializable;

public class Event implements Serializable{
	private int id;
	private String name;
	private String tag;
	private int hostId;
	private String detail;
	private String image;
	private int price;
	private String startDate;
	private String endDate;

	private String sD;
	private String eD;

	public Event() {

	}

	//全てのデータをセットするコンストラクタ
	public Event(int id, String name, String tag, int hostId, String detail, String image, int price, String startDate,
			String endDate) {
		this.id = id;
		this.name = name;
		this.tag = tag;
		this.hostId = hostId;
		this.detail = detail;
		this.image = image;
		this.price = price;
		this.setStartDate(startDate);
		this.setEndDate(endDate);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}


	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}


	public int getHostId() {
		return hostId;
	}

	public void setHostId(int hostId) {
		this.hostId = hostId;
	}

	public String geteD() {
		return eD;
	}

	public void seteD(String eD) {
		this.eD = eD;
	}

	public String getsD() {
		return sD;
	}

	public void setsD(String sD) {
		this.sD = sD;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


}
