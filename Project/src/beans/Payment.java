package beans;

import java.io.Serializable;
import java.util.Date;

public class Payment implements Serializable{
	private int id;
	private int eventId;
	private int userId;
	private int payment;
	private Date paymentDate;

	//全てのデータをセットするコンストラクタ
	public Payment(int id, int eventId, int userId, int payment, Date paymentDate) {
		this.id=id;
		this.eventId=eventId;
		this.userId=userId;
		this.payment=payment;
		this.paymentDate=paymentDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPayment() {
		return payment;
	}

	public void setPayment(int payment) {
		this.payment = payment;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

}
