package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
	private int id;
	private int levelId;
	private String levelName;
	private String loginId;
	private String name;
	private String password;
	private String mailAddress;
	private String postalCode;
	private String address;
	private String tel;
	private int money;
	private Date createDate;
	private Date updateDate;

	public User() {

	}

	//ログイン情報をセットするコンストラクタ
	public User(int id,int levelId,String loginId,String name,int money) {
		this.id=id;
		this.levelId=levelId;
		this.loginId=loginId;
		this.name=name;
		this.money=money;
	}

	//全てのデータをセットするコンストラクタ
	public User(int id, int levelId, String levelName,String loginId, String name, String password, String mailAddress,
			String postalCode, String address, String tel, int money, Date createDate, Date updateDate) {
		this.id = id;
		this.levelId = levelId;
		this.levelName = levelName;
		this.loginId = loginId;
		this.name = name;
		this.password = password;
		this.mailAddress = mailAddress;
		this.postalCode = postalCode;
		this.address = address;
		this.tel = tel;
		this.money = money;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLevelId() {
		return levelId;
	}

	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public String getFormatMoney() {
		return String.format("%,d", this.money);
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
