<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">
					<!--ユーザーリストはjspで制御-->
					<li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }">
						<li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li>
					</c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<main>
	<div class="main-content">
		<div class="detail-user">
			<div class="title-group">
				<h1 class="title">ユーザー詳細</h1>
			</div>
			<div class="make-space-top-bottom">
				<table class="user-table" align="center">
					<!-- jspでアカウントレベルの表示を制御-->
					<tr>
						<th>アカウントレベル</th>
						<td>${dUser.levelName }</td>
					</tr>
					<tr>
						<th>ログインID</th>
						<td>${dUser.loginId }</td>
					</tr>
					<tr>
						<th>ユーザー名</th>
						<td>${dUser.name }</td>
					</tr>
					<tr>
						<th>メールアドレス</th>
						<td>${dUser.mailAddress }</td>
					</tr>
					<tr>
						<th>郵便番号</th>
						<td>${dUser.postalCode }</td>
					</tr>
					<tr>
						<th>住所</th>
						<td>${dUser.address }</td>
					</tr>
					<tr>
						<th>電話番号</th>
						<td>${dUser.tel }</td>
					</tr>
					<tr>
						<th>所持金</th>
						<td>${uMoney }円</td>
					</tr>
					<tr>
						<th>登録日時</th>
						<td>${dUser.createDate }</td>
					</tr>
					<tr>
						<th>更新日時</th>
						<td>${dUser.updateDate }</td>
					</tr>
				</table>
			</div>
			<!--  jspでボタン表示を制御　-->
			<div class="make-space-top-bottom">
				<form class="select-button-group" action="EditUserForAdmin" method="get">
					<input type="hidden" name="id" value="${dUser.id }">
					<button class="btn-square-little-rich-green">　編集　</button>
				</form>
				<c:if test="${user.levelId==2&&dUser.id!=user.id}">
					<form class="select-button-group" action="DeleteUser" method="get">
						<input type="hidden" name="id" value="${dUser.id }">
						<button class="btn-square-little-rich-orange">　削除　</button>
					</form>
				</c:if>
				<br>
                    <br>
                    <a href="IndexUser">
						<button class="btn-square-little-rich-gray">ユーザー一覧に戻る</button>
					</a>
			</div>

		</div>
	</div>


	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>

	</footer>
</body>
</html>
