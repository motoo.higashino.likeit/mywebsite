<!DOCTYPE html>
<link rel="stylesheet" href="materialize.css">

<!-- jspでifを使ってログインセッションがある場合のみ表示-->
<div class="header-content">
    <div class="header-title">
        <h3>
            イベントツール
        </h3>
    </div>
    <nav>
        <ul class="header-link">
            <li><a href="">ユーザー詳細</a></li>
            <li><a href="">ログアウト</a></li>
        </ul>
    </nav>
</div>
