<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">
					<!--ユーザーリストはjspで制御-->
					<li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }">
						<li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li>
					</c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<main>
	<div class="main-content">
		<div class="event-all">
			<div class="detail-event">
				<div class="title-group">
					<h1 class="title">イベント作成</h1>
				</div>
				<h3 class="caution">${errMsg }</h3>
				<form action="RegistEvent" method="post" enctype="multipart/form-data">
					<div class="make-space-top-bottom">

						<table class="detail-table" align="center">
							<tr>
								<th>イベント名</th>
								<td><input type="text" name="eName" value="${rEvent.name }"></td>
							</tr>
							<tr>
								<th>イベントタグ</th>
								<td><input type="text" name="eTag" value="${rEvent.tag }"></td>
							</tr>
							<tr>
								<th>画像</th>
								<td><input type="file" name="eImage"></td>
							</tr>
							<tr>
								<th>主催者</th>
								<td>${user.name }<input type="hidden" name="eHostId"
									value="${user.id }">
								</td>
							</tr>
							<tr>
								<th>開催日時</th>
								<td><input type="datetime-local" name="eSD"
									value="${rEvent.sD}">～ <input type="datetime-local"
									name="eED" value="${rEvent.eD }"></td>
							</tr>
							<tr>
								<th>参加費</th>
								<td><input type="text" name="ePrice"
									value="${rEvent.price }">円</td>
							</tr>
							<tr>
								<th>詳細</th>
								<td><textarea rows="10" cols="60" name="eDetail">${rEvent.detail }</textarea></td>
							</tr>
						</table>
					</div>
					<!--  jspでボタン表示を制御　-->
					<div class="make-space-top-bottom">
						<button class="btn-square-little-rich-blue">　　作成　　</button>
					</div>
				</form>
				<a class="select-button-group" href="IndexEvent">
					<button class="btn-square-little-rich-orange">キャンセル</button>
				</a>
			</div>
		</div>
	</div>
	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>

	</footer>
</body>
</html>
