<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">
					<!--ユーザーリストはjspで制御-->
					<li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }">
						<li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li>
					</c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<main>
	<div class="main-content">
		<div class="detail-user">
			<div class="title-group">
				<h1 class="title">登録内容変更</h1>
			</div>
			<div class="make-space-top-bottom">
				<h3 class="caution">変更内容をご確認下さい</h3>
				<form>
					<table class="user-table" align="center">
						<!-- jspでアカウントレベルの表示を制御-->
						<tr>
							<th>ログインID</th>
							<td>${eUser.loginId }</td>
						</tr>
						<tr>
							<th>ユーザーレベル</th>
							<td>${eUser.levelId }</td>
						</tr>
						<tr>
							<th>ユーザー名</th>
							<td>${eUser.name }</td>
						</tr>
						<tr>
							<th>パスワード</th>
							<td>${passwordStatus }</td>
						</tr>
						<tr>
							<th>メールアドレス</th>
							<td>${eUser.mailAddress }</td>
						</tr>
						<tr>
							<th>郵便番号</th>
							<td>${eUser.postalCode }</td>
						</tr>
						<tr>
							<th>住所</th>
							<td>${eUser.address }</td>
						</tr>
						<tr>
							<th>電話番号</th>
							<td>${eUser.tel }</td>
						</tr>
					</table>
				</form>
			</div>
			<!--  jspでボタン表示を制御　-->
			<div class="make-space-top-bottom">
				<h3 class="caution">この内容でよろしいですか？</h3>
				<form class="select-button-group" action="EditUserResult"
					method="post">
					<button class="btn-square-little-rich-blue">更新する</button>
				</form>
				<a class="select-button-group" href="EditUserForAdmin">
					<button class="btn-square-little-rich-green">修正する</button>
				</a>
			</div>

		</div>
	</div>


	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>

	</footer>
</body>

</html>
