<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">

				</ul>
			</nav>
		</div>
	</header>
	<main>
	<div class="main-content">
		<div class="login-title">
			<h1 class="title">ログイン</h1>
		</div>
		<div class="login-input">
			<h3 class="caution">${errMsg }</h3>
			<h3>${logoutMsg }</h3>
			<form action="Login" method="post">
				<p>ログインID</p>
				<input type="text" name="loginId" value="${loginId }">
				<p>パスワード</p>
				<input type="password" name="password"> <br>
				<br>
				<br>
				<button class="btn-square-little-rich-blue" type="submit">　　　ログイン　　　</button>
			</form>

		</div>
		<div class="align-right">
			<p>
				<a class="btn-square-little-rich-green" href="RegistUser">新規登録</a>
			</p>
		</div>
	</div>

	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>

	</footer>
</body>
</html>
