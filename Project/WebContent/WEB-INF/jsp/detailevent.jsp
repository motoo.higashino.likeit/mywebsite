<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">
					<!--ユーザーリストはjspで制御-->
					<li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }">
						<li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li>
					</c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>

	</header>
	<main>
	<div class="main-content">
		<div class="event-all">
			<div class="detail-event">
				<div class="title-group">
					<h1 class="title">${event.name }</h1>
				</div>
				<div>
					<img src="img/${event.image }">
				</div>
				<div class="make-space-top-bottom">
					<table class="detail-table" align="center">
						<tr>
							<th>イベント名</th>
							<td>${event.name }</td>
						</tr>
						<tr>
							<th>イベントタグ</th>
							<td>${event.tag }</td>
						</tr>
						<tr>
							<th>主催者</th>
							<td>${uName }</td>
						</tr>
						<tr>
							<th>開催日時</th>
							<td>${event.sD }～${event.eD }</td>
						</tr>
						<tr>
							<th>参加費</th>
							<td>${event.price }円</td>
						</tr>
						<tr>
							<th>詳細</th>
							<td>${event.detail }</td>
						</tr>
					</table>
				</div>
				<!--  jspでボタン表示を制御　-->
				<div class="make-space-top-bottom">
					<c:if test="${joinStatus==false&&user.id!=event.hostId}">
						<p class="select-button-group">
							<a class="btn-square-little-rich-blue"
								href="JoinEvent?id=${event.id }"> 参加 </a>
						</p>
					</c:if>
					<c:if test="${user.id==event.hostId||user.levelId==2 }">
						<p class="select-button-group">
							<a class="btn-square-little-rich-green"
								href="EditEvent?id=${event.id }"> 編集 </a>
						</p>
					</c:if>
					<c:if test="${joinStatus==true }">
						<p class="select-button-group">
							<a href="CancelEvent?id=${event.id }">
								<button class="btn-square-little-rich-orange">キャンセル</button>
							</a>
						</p>
					</c:if>
					<br> <br> <a href="IndexEvent">
						<button class="btn-square-little-rich-gray">イベント一覧に戻る</button>
					</a>

				</div>
				<div>
					<c:if test="${user.id==event.hostId }">


						<h3 class="mini-title">参加者</h3>

						<c:if test="${uList==null }">
							<h3>参加者はまだいません</h3>
						</c:if>
						<c:if test="${uList!=null }">
							<table class="list-table">
								<tr>
									<th>ログインID</th>
									<th>ユーザー名</th>
									<th>メールアドレス</th>
									<th>電話番号</th>
								</tr>
								<c:forEach var="u" items="${uList }">
									<tr>
										<td>${u.loginId }</td>
										<td>${u.name }</td>
										<td>${u.mailAddress }</td>
										<td>${u.tel }</td>
									</tr>
								</c:forEach>
							</table>
						</c:if>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>

	</footer>
</body>
</html>
