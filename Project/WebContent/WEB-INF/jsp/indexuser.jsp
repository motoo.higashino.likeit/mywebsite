<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">
					<!--ユーザーリストはjspで制御-->
					<li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }">
						<li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li>
					</c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<main>
	<div class="main-content">
		<div class="indexuser-content">
			<div class="title-gruop">
				<h1 class="title">ユーザー一覧</h1>
			</div>
			<div class="index-method">
				<h3 class="mini-title">ユーザー検索</h3>
				<form action="IndexUser" method="post">
					<table class="search-table" align="center">
						<tr>
							<th><label>ログインID</label></th>
							<td><input type="text" name="loginId"></td>
						</tr>
						<tr>
							<th><label>ユーザー名</label></th>
							<td><input type="text" name="userName"></td>
						</tr>
						<tr>
							<th><label>メールアドレス</label></th>
							<td><input type="text" name="mailAddress"></td>
						</tr>
					</table>
					<p>
					<button class="btn-square-little-rich-gray" type="submit">　　検索　　</button>
					</p>
				</form>


			</div>

			<div>
				<table class="list-table">
					<tr>
						<th>ログインID</th>
						<th>アカウントレベル</th>
						<th>ユーザー名</th>
						<th>メールアドレス</th>
						<th>電話番号</th>
						<th>所持金額</th>
						<th></th>

					</tr>
					<c:forEach var="u" items="${uList }">
						<tr>
							<td>${u.loginId }</td>
							<td>${u.levelName }</td>
							<td>${u.name }</td>
							<td>${u.mailAddress }</td>
							<td>${u.tel }</td>
							<td>${u.formatMoney } 円</td>
							<td><a class="btn-square-little-rich-blue"
								href="DetailUserForAdmin?id=${u.id }">詳細</a>
							<a class="btn-square-little-rich-green"
								href="EditUserForAdmin?id=${u.id }">更新</a>
							<c:if test="${user.id!=u.id&&u.levelId!=2||user.id==1}">
								<a class="btn-square-little-rich-orange"
									href="DeleteUser?id=${u.id }">削除</a>
							</c:if>
							</td>

						</tr>
					</c:forEach>
				</table>
			</div>
		</div>




	</div>

	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>

	</footer>
</body>
</html>
