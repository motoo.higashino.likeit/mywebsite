<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UFT-8">
    <link href="css/materialize.css" type="text/css" rel="stylesheet">
    <title></title>
</head>

<body>
    <header>
        <div class="header-content">
            <div class="header-title">
                <h3>
                    イベントツール
                </h3>
            </div>
            <nav>
                <ul class="header-link">
                    <!--ユーザーリストはjspで制御-->
                   <li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }"><li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li></c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <main>
        <div class="main-content">
            <div class="event-all">
                <div class="detail-event">
                    <h2 class="caution">作成内容をご確認下さい</h2>
                    <div class="title-group">
                        <h1 class="title">イベント名</h1>
                    </div>
                    <div>
                        <img src="img/${rEvent.image }">
                    </div>
                    <div class="make-space-top-bottom">
                        <table class="detail-table" align="center">
                            <tr>
                                <th>イベント名</th>
                                <td>${rEvent.name }</td>
                            </tr>
                            <tr>
                                <th>イベントタグ</th>
                                <td>${rEvent.tag }</td>
                            </tr>
                            <tr>
                                <th>主催者</th>
                                <td>${user.name }</td>
                            </tr>
                            <tr>
                                <th>開催日時</th>
                                <td>${rEvent.sD }～${rEvent.eD }</td>
                            </tr>
                            <tr>
                                <th>参加費</th>
                                <td>${rEvent.price }円</td>
                            </tr>
                            <tr>
                                <th>詳細</th>
                                <td>${rEvent.detail }</td>
                            </tr>
                        </table>
                    </div>
                    <!--  jspでボタン表示を制御　-->
                    <div class="make-space-top-bottom" >
                        <h3 class="caution">この内容でよろしいですか？</h3>
                        <form class="select-button-group" action="RegistEventResult" method="post">
                            <button class="btn-square-little-rich-blue" >　作成　</button>
                        </form>
                        <form class="select-button-group" action="RegistEvent" method="get">
                            <button class="btn-square-little-rich-green">　修正　</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <div class="footer-logo">
            <h3>
                MADE BY MH
            </h3>
        </div>

    </footer>
</body></html>
