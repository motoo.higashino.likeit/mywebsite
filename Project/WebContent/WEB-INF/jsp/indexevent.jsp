<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">
					<!--ユーザーリストはjspで制御-->
					<li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }">
						<li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li>
					</c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<div align="right"><p class="user-status"><b>${user.name }　さん　　所持金：${uMoney } 円</b></p></div>
	<main>
	<div class="main-content">

		<div class="indexevent-content">
			<div class="title-group">
				<h1 class="title">イベントツール</h1>
			</div>
			<div class="align-right">
				<a href="RegistEvent" class="btn-square-little-rich-gray">
					新しいイベントを作成する </a>
			</div>
			<c:if test="${heList != null }">
				<div class="user-hosted-list">
					<h4 class="mini-title">主催しているイベント</h4>
					<table class="list-table">
						<tr>
							<th></th>
							<th>イベント名</th>
							<th>イベントタグ</th>
							<th>開始日時</th>
							<th>終了日時</th>
							<th></th>
						</tr>
						<c:forEach var="he" items="${heList }">
							<tr>
								<td><a class="" href="DetailEvent?id=${he.id}"><img
										class="image" alt="" src="img/${he.image }" width="100"
										height="70"></a></td>
								<td><a class="title-link" href="DetailEvent?id=${he.id}"><font
										color="#000" size="5"><b>${he.name }</b></font></a>
								<td>${he.tag }</td>
								<td>${he.sD }</td>
								<td>${he.eD }</td>
								<td><a class="btn-square-little-rich-green"
									href="DetailEvent?id=${he.id}">詳細</a> <a
									class="btn-square-little-rich-gray"
									href="EditEvent?id=${he.id}">編集</a> <c:if
										test="${user.levelId==2||user.id==he.hostId }">
										<a class="btn-square-little-rich-orange"
											href="DeleteEvent?id=${he.id}">削除</a>
										<td>
									</c:if>
							</tr>
						</c:forEach>
					</table>
				</div>
				<br>
				<br>
			</c:if>
			<c:if test="${jeList != null }">
				<div class="user-booked-list">
					<!-- jspでif使う-->
					<h4 class="mini-title">参加予定イベント</h4>

					<table class="list-table">
						<tr>
							<th></th>
							<th>イベント名</th>
							<th>イベントタグ</th>
							<th>開始日時</th>
							<th>終了日時</th>
							<th></th>

						</tr>
						<c:forEach var="je" items="${jeList }">
							<tr>
								<td><a class="" href="DetailEvent?id=${je.id }"><img
										class="image" alt="" src="img/${je.image }" width="100"
										height="70"></a></td>
								<td><a class="title-link" href="DetailEvent?id=${je.id}"><font
										color="#000" size="5"><b>${je.name }</b></font></a></td>
								<td>${je.tag }</td>
								<td>${je.sD }</td>
								<td>${je.eD }</td>
								<td><a class="btn-square-little-rich-green"
									href="DetailEvent?id=${je.id }">詳細</a></td>
							</tr>
						</c:forEach>
					</table>

				</div>
				<br>
				<br>
			</c:if>
			<div class="search-event">
				<h4 class="mini-title">イベント検索</h4>
				<form action="IndexEvent" method="post">
					<table class="search-table" align="center">
						<tr>
							<th><label>イベント名</label></th>
							<td><input type="text" name="eventName"></td>
						</tr>
						<tr>
							<th><label>イベントタグ</label></th>
							<td><input type="text" name="eventTag"></td>

						</tr>
						<tr>
							<th><label>開催日時</label></th>
							<td><input type="datetime-local" name="startDate">～<input
								type="datetime-local" name="endDate"></td>
						</tr>
					</table>
					<p>
						<button class="btn-square-little-rich-gray" type="submit">
							　　　検索　　　</button>
					</p>
				</form>
			</div>
			<div class="unbooked-list">
				<h4 class="mini-title">イベント</h4>
				<c:if test="${wheList == null }">
					<h3>イベントはありません</h3>
				</c:if>
				<c:if test="${wheList != null }">
					<table class="list-table">
						<tr>
							<th></th>
							<th>イベント名</th>
							<th>イベントタグ
							<th>開始日時</th>
							<th>終了日時</th>
							<th>参加費</th>
							<th>詳細</th>
							<th></th>

						</tr>
						<c:forEach var="whe" items="${wheList }">
							<tr>
								<td><a class="" href="DetailEvent?id=${whe.id}"><img
										class="image" alt="" src="img/${whe.image }" width="100"
										height="70"></a></td>
								<td><a class="title-link" href="DetailEvent?id=${whe.id}"><font
										color="#000" size="5"><b>${whe.name }</b></font></a></td>
								<td>${whe.tag }</td>
								<td>${whe.sD }</td>
								<td>${whe.eD }</td>
								<td>${whe.formatPrice } 円</td>
								<td>${whe.detail }</td>
								<td><a class="btn-square-little-rich-green"
									href="DetailEvent?id=${whe.id}">詳細</a> <a
									class="btn-square-little-rich-blue"
									href="JoinEvent?id=${whe.id}">参加</a> <c:if
										test="${user.levelId==2||user.id==whe.hostId }">
										<a class="btn-square-little-rich-orange"
											href="DeleteEvent?id=${whe.id}">削除</a>
										<td>
									</c:if>
							</tr>
						</c:forEach>
					</table>
				</c:if>
			</div>
		</div>
	</div>
	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>


	</footer>
</body>
</html>
