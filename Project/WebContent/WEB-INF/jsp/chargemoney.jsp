<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">
					<!--ユーザーリストはjspで制御-->
					<li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }">
						<li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li>
					</c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<main>
	<div class="main-content">
		<div class="charge-money">
			<div class="title-group">
				<h1 class="title">お財布チャージ</h1>
			</div>
			<h3 class="caution">${errMsg }</h3>
			<div>
				<table class="bottom-line-table" align="center">
					<tr>
						<th>所持金</th>
						<td>${uMoney }円</td>

					</tr>

				</table>
			</div>
			<div class="make-space-top-bottom">
				<form action="ChargeMoney" method="post">
					<table class="charge-table" align="center">
						<tr>
							<th><input type="radio" name="cMoney" value="1000"></th>
							<td>1,000円</td>
						</tr>
						<tr>
							<th><input type="radio" name="cMoney" value="3000"></th>
							<td>3,000円</td>
						</tr>
						<tr>
							<th><input type="radio" name="cMoney" value="5000"></th>
							<td>5,000円</td>
						</tr>
						<tr>
							<th><input type="radio" name="cMoney" value="10000"></th>
							<td>10,000円</td>
						</tr>
					</table>
					<br> <button class="btn-square-little-rich-blue" type="submit">チャージ</button>
				</form>
				<br> <br> <a href="IndexEvent">
					<button class="btn-square-little-rich-gray">イベント一覧に戻る</button>
				</a>
			</div>
		</div>
	</div>
	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>

	</footer>
</body>
</html>
