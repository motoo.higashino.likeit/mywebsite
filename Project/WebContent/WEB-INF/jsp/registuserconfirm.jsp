<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UFT-8">
    <link href="css/materialize.css" type="text/css" rel="stylesheet">
    <title></title>
</head>

<body>
    <header>
        <div class="header-content">
            <div class="header-title">
                <h3>
                    イベントツール
                </h3>
            </div>
            <nav>
                <ul class="header-link">
                    <!--ユーザーリストはjspで制御-->
                </ul>
            </nav>
        </div>
    </header>
    <main>
        <div class="main-content">
            <div class="detail-user">
                <div class="title-group">
                    <h1 class="title">ユーザー新規登録</h1>
                </div>
                <div class="make-space-top-bottom">
                    <h3 class="caution">ご登録内容をご確認下さい</h3>
                    <form>
                        <table class="user-table" align="center">
                            <!-- jspでアカウントレベルの表示を制御-->
                                  <tr>
                            <th>ログインID</th>
                            <td>${rUser.loginId }</td>
                        </tr>
                        <tr>
                            <th>ユーザー名</th>
                            <td>${rUser.name }</td>
                        </tr>
                             <tr>
                            <th>パスワード</th>
                            <td>${rUser.password }</td>
                        </tr>
                        <tr>
                            <th>メールアドレス</th>
                            <td>${rUser.mailAddress }</td>
                        </tr>
                        <tr>
                            <th>郵便番号</th>
                            <td>${rUser.postalCode }</td>
                        </tr>
                        <tr>
                            <th>住所</th>
                            <td>${rUser.address }</td>
                        </tr>
                        <tr>
                            <th>電話番号</th>
                            <td>${rUser.tel }</td>
                        </tr>
                        </table>
                    </form>
                </div>
                <div class="make-space-top-bottom">
                <form class="select-button-group" action="RegistUser" method="get">
                        <button class="btn-square-little-rich-green">　修正する　</button>
                    </form>
                    <form class="select-button-group" action="RegistUserResult" method="post">
                        <button class="btn-square-little-rich-blue">　登録する　</button>
                    </form>
                </div>

            </div>
        </div>


    </main>
    <footer>
        <div class="footer-logo">
            <h3>
                MADE BY MH
            </h3>
        </div>

    </footer>
</body>

</html>
