<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UFT-8">
    <link href="css/materialize.css" type="text/css" rel="stylesheet">
    <title></title>
</head>

<body>
    <header>
        <div class="header-content">
            <div class="header-title">
                <h3>
                    イベントツール
                </h3>
            </div>
            <nav>
                <ul class="header-link">
                    <!--ユーザーリストはjspで制御-->
                    <li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }"><li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li></c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <main>
        <div class="main-content">
            <div class="charge-money-result">
                <div class="title-group">
                    <h1 class="title">
                        予約キャンセル
                    </h1>
                </div>
                <div class="">
                    <h3>キャンセルが完了しました</h3>
                    <table class="simple-table" align="center">
                           <tr>
                            <th>イベント名</th>
                            <td>${cEvent.name }</td>
                        </tr>
                    </table>
                </div>
                <br>
                <div class="make-space-top-bottom">
                    <div>
                        <form class="select-button-group" action="IndexEvent" method="get">
                            <button class="btn-square-little-rich-gray">イベント一覧に戻る</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </main>
    <footer>
        <div class="footer-logo">
            <h3>
                MADE BY MH
            </h3>
        </div>

    </footer>
</body>

</html>
