<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UFT-8">
    <link href="css/materialize.css" type="text/css" rel="stylesheet">
    <title></title>
</head>

<body>
    <header>
        <div class="header-content">
            <div class="header-title">
                <h3>
                    イベントツール
                </h3>
            </div>
            <nav>
                <ul class="header-link">
                    <!--ユーザーリストはjspで制御-->
                </ul>
            </nav>
        </div>
    </header>
    <main>
        <div class="main-content">
            <div class="charge-money-result">
                <div class="title-group">
                    <h1 class="title">
                        ユーザー新規登録
                    </h1>
                </div>
                <div class="">
                    <h3>登録が完了しました</h3>
                    <h3>ようこそ　${user.name } さん</h3>
                </div>
                <br>
                <div>
                    <div>
                        <a href="IndexEvent">
                        <button class="btn-square-little-rich-blue">イベント一覧を見る</button>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </main>
    <footer>
        <div class="footer-logo">
            <h3>
                MADE BY MH
            </h3>
        </div>

    </footer>
</body>

</html>
