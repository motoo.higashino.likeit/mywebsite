<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">
					<!--ユーザーリストはjspで制御-->

				</ul>
			</nav>
		</div>
	</header>
	<main>
	<div class="main-content">
		<div class="detail-user">
			<div class="title-group">
				<h1 class="title">ユーザー新規登録</h1>
			</div>
			<h3 class="caution">${errMsg }</h3>
			<form action="RegistUser" method="post">
				<div class="make-space-top-bottom">

					<table class="user-table" align="center">
						<tr>
							<th>ログインID</th>
							<td><input type="text" name="loginId"
								value="${rUser.loginId }"></td>
						</tr>
						<tr>
							<th>ユーザー名</th>
							<td><input type="text" name="UserName"
								value="${rUser.name }"></td>
						</tr>
						<tr>
							<th>パスワード</th>
							<td><input type="password" name="password1"></td>
						</tr>
						<tr>
							<th>パスワード(確認)</th>
							<td><input type="password" name="password2"></td>
						</tr>
						<tr>
							<th>メールアドレス</th>
							<td><input type="text" name="mailAddress1"
								value="${rUser.mailAddress }"></td>
						</tr>
						<tr>
							<th>メールアドレス(確認)</th>
							<td><input type="text" name="mailAddress2"></td>
						</tr>
						<tr>
							<th>郵便番号</th>
							<td><input type="text" name="postalCode"
								value="${rUser.postalCode }"></td>
						</tr>
						<tr>
							<th>住所</th>
							<td><input type="text" name="address"
								value="${rUser.address }"></td>
						</tr>
						<tr>
							<th>電話番号</th>
							<td><input type="text" name="tel" value="${rUser.tel }"></td>
						</tr>
					</table>
				</div>
				<!--  jspでボタン表示を制御　-->
				<div class="make-space-top-bottom">
					<div class="select-button-group">
						<button class="btn-square-little-rich-blue" type="submit">
							新規登録</button>

					</div>

				</div>
			</form>
			<a href="Login">
				<button class="btn-square-little-rich-gray">ログイン画面へ戻る</button>
			</a>
		</div>
	</div>


	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>

	</footer>
</body>

</html>
