<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UFT-8">
<link href="css/materialize.css" type="text/css" rel="stylesheet">
<title></title>
</head>

<body>
	<header>
		<div class="header-content">
			<div class="header-title">
				<h3>イベントツール</h3>
			</div>
			<nav>
				<ul class="header-link">
					<!--ユーザーリストはjspで制御-->
					<li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }">
						<li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li>
					</c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
				</ul>
			</nav>
		</div>
	</header>
	<main>
	<div class="main-content">
		<div class="detail-user">
			<div class="title-group">
				<h1 class="title">登録内容変更</h1>
			</div>
			<h3 class="caution">${errMsg }</h3>
			<form class="" action="EditUser" method="post">
				<div class="make-space-top-bottom">

					<table class="user-table" align="center">
						<!-- jspでアカウントレベルの表示を制御-->
						<tr>
							<th>ログインID</th>
							<td>${eUser.loginId }</td>
						</tr>
						<c:if test="${user.id==1 }">
						<tr>
						<th>ユーザーレベル</th>
						<td>${eUser.levelName }</td>
						</tr>
						</c:if>
						<tr>
							<th>ユーザー名</th>
							<td><input type="text" name="name" value="${eUser.name }"></td>
						</tr>
						<tr>
							<th>パスワード</th>
							<td><input type="password" name="password1"></td>
						</tr>
						<tr>
							<th>パスワード(確認)</th>
							<td><input type="password" name="password2"></td>
						</tr>
						<tr>
							<th>メールアドレス</th>
							<td><input type="text" name="mailAddress1"
								value="${eUser.mailAddress }"></td>
						</tr>
						<tr>
							<th>メールアドレス(確認)</th>
							<td><input type="text" name="mailAddress2"
								value="${eUser.mailAddress }"></td>
						</tr>
						<tr>
							<th>郵便番号</th>
							<td><input type="text" name="postalCode"
								value="${eUser.postalCode }"></td>
						</tr>
						<tr>
							<th>住所</th>
							<td><input type="text" name="address"
								value="${eUser.address }"></td>
						</tr>
						<tr>
							<th>電話番号</th>
							<td><input type="text" name="tel" value="${eUser.tel }"></td>
						</tr>
					</table>
				</div>
				<!--  jspでボタン表示を制御　-->
				<div class="make-space-top-bottom">
				<button class="btn-square-little-rich-green">　変更　</button>
				</div>
				</form>
			<a class="select-button-group" href="DetailUser">
				<button class="btn-square-little-rich-orange">　戻る　</button>
			</a>

		</div>
	</div>


	</main>
	<footer>
		<div class="footer-logo">
			<h3>MADE BY MH</h3>
		</div>

	</footer>
</body>

</html>
