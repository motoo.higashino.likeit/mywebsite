<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UFT-8">
    <link href="css/materialize.css" type="text/css" rel="stylesheet">
    <title></title>
</head>

<body>
    <header>
        <div class="header-content">
            <div class="header-title">
                <h3>
                    イベントツール
                </h3>
            </div>
            <nav>
                <ul class="header-link">
                   <!--ユーザーリストはjspで制御-->
                    <li><a class="btn-border-bottom" href="IndexEvent">イベントリスト</a></li>
					<c:if test="${user.levelId==2 }"><li><a class="btn-border-bottom" href="IndexUser">ユーザーリスト</a></li></c:if>
					<li><a class="btn-border-bottom" href="ChargeMoney">お財布チャージ</a></li>
					<li><a class="btn-border-bottom" href="DetailUser">ユーザー詳細</a></li>
					<li><a class="btn-border-bottom" href="Logout">ログアウト</a></li>
                </ul>
            </nav>
        </div>
    </header>
    <main>
        <div class="main-content">
            <div class="charge-money-result">
                <div class="title-group">
                    <h1 class="title">
                       イベント作成
                    </h1>
                </div>
                <div class="">
                    <h3>${eName }を作成しました</h3>
                    <h3>SNSで参加者を集めましょう</h3>
                </div>
                <div>
                    <div>
                        <a href="IndexEvent">
                        <button class="btn-square-little-rich-gray">イベント一覧に戻る</button>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </main>
    <footer>
        <div class="footer-logo">
            <h3>
                MADE BY MH
            </h3>
        </div>

    </footer>
</body>

</html>
