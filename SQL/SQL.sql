CREATE DATABASE eventmanagement_db DEFAULT CHARACTER SET utf8;

USE eventmanagement_db;

CREATE TABLE t_event_list(
id int primary key AUTO_INCREMENT,
event_name varchar(256) NOT NULL,
event_tag varchar(256) NOT NULL,
host_id int NOT NULL,
detail varchar(600) NOT NULL,
image varchar(256) NOT NULL,
price int NOT NULL,
start_date DATETIME NOT NULL,
end_date DATETIME NOT NULL
);

CREATE TABLE t_user_list(
id int primary key AUTO_INCREMENT,
level_id int NOT NULL,
login_id varchar(20) UNIQUE NOT NULL,
user_name varchar(20) NOT NULL,
password varchar(100) NOT NULL,
mail_address varchar(100) NOT NULL,
postal_code varchar(20) NOT NULL,
address varchar(100) NOT NULL,
tel varchar(20) NOT NULL,
money int NOT NULL,
create_date DATETIME NOT NULL,
update_date DATETIME NOT NULL
);

CREATE TABLE t_user_level(
level_id int primary key,
level_name varchar(20) NOT NULL
);

CREATE TABLE t_management_user_event(
id int primary key AUTO_INCREMENT,
user_id int NOT NULL,
event_id int NOT NULL
);

CREATE TABLE t_payment(
id int primary key AUTO_INCREMENT,
event_id int NOT NULL,
user_id int NOT NULL,
payment int NOT NULL,
pay_date date NOT NULL
);



USE eventmanagement_db;

DROP TABLE 
t_event_list,
t_management_user_event,
t_payment,
t_user_level,
t_user_list;



